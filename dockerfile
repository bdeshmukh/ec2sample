# Pull base image
From tomcat:8-jre8

# Maintainer
MAINTAINER "Bdeshmukh <bhdeshmukh@in.ibm.com">

# Copy to images tomcat path
ADD hello_world-1.0-SNAPSHOT.war /usr/local/tomcat/webapps/